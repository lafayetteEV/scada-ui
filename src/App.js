import React, { Component } from 'react';
import { connect } from 'react-redux'

import AppBar from 'material-ui/lib/app-bar'
import Sidebar from './Sidebar.js'
import MainView from './MainView.js'

const sidebarStyle = {
  position: "fixed",
  width: "auto",
  height: "100%"
};

const mainViewStyle = {
    marginLeft: '200px',
    width: 'auto',
};

class App extends Component {

  constructor(props) {
  	super(props);
    this.state = {leftNavOpen: false};
  }

  render() {
    return (
    	<div>
    		<AppBar title="SCADA UI"
          showMenuIconButton={true}/>
	    	<Sidebar style={sidebarStyle} subsystems={this.props.subsystems}/>
        <MainView style={mainViewStyle} subsystems={this.props.subsystems}/>
    	</div>
    );
  }
}

function select(state) {
  return state;
}

export default connect(select)(App);
