import { combineReducers } from 'redux'

var initialState = {
  subsystems: {
    "motor_controller_0": {type: "subsystem", systype: "motor_controller", name:"motor_controller_0"},
    "battery_pack_0": {type: "subsystem", systype: "battery_pack", name:"battery_pack_0"},
    "battery_pack_1": {type: "subsystem", systype: "battery_pack", name:"battery_pack_1"},
    "battery_pack_2": {type: "subsystem", systype: "battery_pack", name:"battery_pack_2"},
    "Dashboard": {type: "subsystem", systype: "Dashboard" , name:"Dashboard", virtual: true}
  }
}

function subsystemReducer(state = initialState, action) {
  switch (action) {
    case "SUBSYSTEM_UPDATE":
      return state;
    break;
  default:
    return state;
  }
}

function wsReducer(state = initialState, action) {
  // if (state.ws === undefined) {
  //   Object.assign(state, {}, {
  //
  //   });
  // }
}

const rootReducer = subsystemReducer;

export default rootReducer
