import React, { Component } from 'react';
import CircularProgress from 'material-ui/lib/circular-progress'

import SubsystemViews from './subsystem-views'

class DefaultSubsystemView extends Component {
  render() {
    return <div>
      <p>Here is an automatically generated view of the subsystem state.</p>
      <code>{JSON.stringify(this.props.data, null, 4)}</code>
    </div>
  }
}

export default class MainView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hash: location.hash.replace('#', '')
    }
  }
  handleHashChange() {
    console.log('hashchange');
    this.setState({
      hash: location.hash.replace('#', '')
    })
  }
  componentDidMount() {
    window.onhashchange = this.handleHashChange.bind(this);
  }
  componentWillUnmount() {
    window.onhashchange = null;
  }
  render() {
    // Check if the subsystem exists
    var subsystem = this.props.subsystems[this.state.hash];
    if (subsystem !== undefined) {
      // Does it have a view?
      console.log(subsystem.systype)
      var SubsystemView = SubsystemViews[subsystem.systype];
      console.log(SubsystemView);
      if (SubsystemView === undefined) {
        SubsystemView = DefaultSubsystemView;
      }
      return <div style={this.props.style}>
        <SubsystemView data={subsystem}/>
      </div>
    }

    // Doesn't exist.
    return <div style={this.props.style}>
      <CircularProgress size={3}/>
    </div>
  }
}
