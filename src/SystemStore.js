
import {EventEmitter} from 'events';

var system = {
  virtual: [
    "Battery Manager",
    "System Status",
    "Dashboard"
  ],
  physical: [
    "motor_controller_0",
    "battery_pack_0",
    "battery_pack_1",
    "battery_pack_2",
    "battery_pack_3"
  ],
};

class SystemStore extends EventEmitter {
  getState() {
    return system;
  }
}

var ss = new SystemStore();

export default ss;
