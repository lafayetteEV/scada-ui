import React, { Component } from 'react';

import List from 'material-ui/lib/lists/list'
import ListItem from 'material-ui/lib/lists/list-item'
import Paper from 'material-ui/lib/paper'
import Divider from 'material-ui/lib/divider'

const style = {
  position: "fixed",
  width: "200px",
  height: "100%"
}

class SubsystemListItem extends Component {
  handleClick(event) {
    location.hash = this.props.data.name
  }

  render() {
    const {name} = this.props.data;
    return <div onClick={this.handleClick.bind(this)}>
      <ListItem>
        {name}
      </ListItem>
    </div>
  }
}

function makeListItem(data) {
  return <SubsystemListItem data={data}/>
}

export default class Sidebar extends Component {

  render() {
    let subsystems = this.props.subsystems;
    let virtual = [];
    let physical = [];
    for (let k of Object.keys(subsystems)) {
      console.log(k)
      let v = subsystems[k];
      if (v.virtual === true) {
        virtual.push(v);
      } else {
        physical.push(v);
      }
    }

    return <Paper style={this.props.style}>
        <List subheader="Virtual">
          {virtual.map(makeListItem)}
        </List>
        <Divider/>
        <List subheader="Physical">
          {physical.map(makeListItem)}
        </List>
      </Paper>
  }
}
